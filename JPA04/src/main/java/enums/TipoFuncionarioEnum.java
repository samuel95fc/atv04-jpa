package enums;

public enum TipoFuncionarioEnum {
	FIXO("FXO"), TEMPORARIO("TMP");
	
	String codigo;
	
	private TipoFuncionarioEnum(String codigo) {
		this.codigo = codigo;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public static TipoFuncionarioEnum valueOfCodigo(String codigo) {
		for (TipoFuncionarioEnum tipoFuncionario : values()) {
			if (tipoFuncionario.getCodigo().equals(codigo)) {
				return tipoFuncionario;
			}
		}
		throw new IllegalArgumentException();
	}
}