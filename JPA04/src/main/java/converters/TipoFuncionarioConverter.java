package converters;

import javax.persistence.AttributeConverter;

import enums.TipoFuncionarioEnum;

public class TipoFuncionarioConverter implements AttributeConverter<TipoFuncionarioEnum, String> {
	
	@Override
	public String convertToDatabaseColumn(TipoFuncionarioEnum attribute) {
		return attribute.getCodigo();
	}

	@Override
	public TipoFuncionarioEnum convertToEntityAttribute(String dbData) {
		return TipoFuncionarioEnum.valueOfCodigo(dbData);
	}

}
