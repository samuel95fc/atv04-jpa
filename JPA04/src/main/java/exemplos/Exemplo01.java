package exemplos;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import domain.Departamento;
import domain.Funcionario;
import enums.TipoFuncionarioEnum;

public class Exemplo01 {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		try {
			emf = Persistence.createEntityManagerFactory("pu1");
			EntityManager em = emf.createEntityManager();

			popularBase(em);
			em.clear();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (emf != null) {
				emf.close();
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	private static void popularBase(EntityManager em) {
		
		em.getTransaction().begin();
		
		Departamento departamento = criarDepartamento(em, "TI", null);
		Funcionario funcionarioJR = criarFuncionario(em, "Samuel", "12345678990", "FXO", new Date(2020, 5, 10), null, departamento);
		Funcionario funcionarioPLENO = criarFuncionario(em, "Claudio", "09876543221", "FXO", new Date(2020, 5, 10), null, departamento);
		
		em.getTransaction().commit();
	}
	
	private static Funcionario criarFuncionario(EntityManager em, String nome, String cpf, TipoFuncionarioEnum situacao, Date dataAdmissao, Date dataDemissao, Departamento departamento) {
		Funcionario funcionario = new Funcionario();
		funcionario.setCpf(cpf);
		funcionario.setDataAdmissao(dataAdmissao);
		funcionario.setDataDemissao(dataDemissao);
		funcionario.setNome(nome);
		funcionario.setSituacao(situacao);
		funcionario.setDepartameto(departameto);
		
		em.persist(funcionario);
		
		return funcionario;
	}
	
	private static Departamento criarDepartamento(EntityManager em, String nome, Funcionario funcionario) {
		Departamento departamento = new Departamento();
		departamento.setNome(nome);
		departamento.setFuncionario(funcionario);
		
		return departamento;
	}
}
