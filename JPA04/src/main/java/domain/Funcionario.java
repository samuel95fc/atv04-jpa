package domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import enums.TipoFuncionarioEnum;

@Entity
@Table(name = "tab_funcionario", uniqueConstraints = {
		@UniqueConstraint(name = "uk_funcionario_cpf",
				columnNames = { "fun_cpf" }) })
@Inheritance(strategy=InheritanceType.JOINED)
public class Funcionario {

	@Id
	@Column(name = "fun_matricula")
	private int matricula;

	@Column(name = "fun_nome", length = 40, nullable = false)
	private String nome;

	@Column(name = "fun_cpf", columnDefinition = "char(11)", nullable = false)
	private String cpf;

	@Convert(converter = TipoFuncionarioEnum.class)
	@Column(name = "fun_tipo", columnDefinition = "char(3)", nullable = false)
	private TipoFuncionarioEnum situacao;

	@Temporal(TemporalType.DATE)
	@Column(name = "fun_dtAdmissao", nullable = false)
	private Date dataAdmissao;

	@Temporal(TemporalType.DATE)
	@Column(name = "fun_dtDemissao", nullable = false)
	private Date dataDemissao;
	
	@ManyToOne(fetch = FetchType.LAZY)
	Departamento departameto;
	
	public Funcionario() {}

	public Funcionario(int matricula, String nome, String cpf, Departamento departameto, TipoFuncionarioEnum situacao,
			Date dataAdmissao, Date dataDemissao) {
		super();
		this.matricula = matricula;
		this.nome = nome;
		this.cpf = cpf;
		this.departameto = departameto;
		this.situacao = situacao;
		this.dataAdmissao = dataAdmissao;
		this.dataDemissao = dataDemissao;
	}

	public int getMatricula() {
		return matricula;
	}

	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Departamento getDepartameto() {
		return departameto;
	}

	public void setDepartameto(Departamento departameto) {
		this.departameto = departameto;
	}

	public TipoFuncionarioEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(TipoFuncionarioEnum situacao) {
		this.situacao = situacao;
	}

	public Date getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(Date dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public Date getDataDemissao() {
		return dataDemissao;
	}

	public void setDataDemissao(Date dataDemissao) {
		this.dataDemissao = dataDemissao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result + ((dataAdmissao == null) ? 0 : dataAdmissao.hashCode());
		result = prime * result + ((dataDemissao == null) ? 0 : dataDemissao.hashCode());
		result = prime * result + ((departameto == null) ? 0 : departameto.hashCode());
		result = prime * result + matricula;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((situacao == null) ? 0 : situacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		if (dataAdmissao == null) {
			if (other.dataAdmissao != null)
				return false;
		} else if (!dataAdmissao.equals(other.dataAdmissao))
			return false;
		if (dataDemissao == null) {
			if (other.dataDemissao != null)
				return false;
		} else if (!dataDemissao.equals(other.dataDemissao))
			return false;
		if (departameto == null) {
			if (other.departameto != null)
				return false;
		} else if (!departameto.equals(other.departameto))
			return false;
		if (matricula != other.matricula)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (situacao != other.situacao)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Funcionario [matricula=" + matricula + ", nome=" + nome + ", cpf=" + cpf + ", departameto="
				+ departameto + ", situacao=" + situacao + ", dataAdmissao=" + dataAdmissao + ", dataDemissao="
				+ dataDemissao + "]";
	}

}