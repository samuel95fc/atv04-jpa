package domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tab_departamento")
@SequenceGenerator(name = "sq_departamento", sequenceName = "sq_departamento", allocationSize = 1)
public class Departamento {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_departamento")
	@Column(name = "dep_codigo", columnDefinition = "char(5)")
	private String codigo;
	
	@Column(name = "dep_nome", length = 40, nullable = false)
	private String nome;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "tab_departamento_funcionario")
	private List<Funcionario> funcionario;
	
	public Departamento() {}
	
	public Departamento(String codigo, String nome, List<Funcionario> funcionario) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.funcionario = funcionario;
	}
	
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Funcionario> getFuncionario() {
		return funcionario;
	}
	
	@SuppressWarnings("unchecked")
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = (List<Funcionario>) funcionario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((funcionario == null) ? 0 : funcionario.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Departamento other = (Departamento) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (funcionario == null) {
			if (other.funcionario != null)
				return false;
		} else if (!funcionario.equals(other.funcionario))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Departamento [codigo=" + codigo + ", nome=" + nome + ", funcionario=" + funcionario + "]";
	}
	
	
}
